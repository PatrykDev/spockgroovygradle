import javax.swing.*;

public class Polygon {

    private final int numberOfSides;
    private final Renderer renderer;

    public Polygon(int numberOfSides, Renderer renderer) throws TooFewSidesException {
        if (numberOfSides <= 2){
            throw new TooFewSidesException();
        }
        this.numberOfSides = numberOfSides;
        this.renderer = renderer;
    }

    public int getNumberOfSides() {
        return numberOfSides;
    }

    public void draw(){
        for (int i = 0; i < numberOfSides; i++) {
            renderer.getComponent();
        }
    }
}
