import spock.lang.Specification

import javax.swing.Renderer

class PolygonTest extends Specification {

    def "should demonstrate given-when-then"(){
        given:
        def polygon = new Polygon(4)

        when:
        int sides = polygon.numberOfSides

        then:
        sides == 4
    }

    def "should expects exception"(){
        when:
        new Polygon(0)

        then:
        def exception = thrown(TooFewSidesException.class)

    }

    def  "should expect exception to be thrown for a number of invalid input: #sides"(){
        when:
        new Polygon(sides)

        then:
        def exception = thrown(TooFewSidesException)

        where:
        sides << [-1, 0, 1, 2]
    }

    def "should be able to create a polygon with #sides sides"(){
        when:
        def polygon = new Polygon(sides)

        then:
        polygon.getNumberOfSides() == sides

        where:
        sides << [3,4,5,6,7,1784]
    }

    def "should use data tables for calculating max"() {
        expect:
        Math.max(a,b) == max

        where:
        a | b  | max
        1 | 2  | 2
        8 | 4  | 8
        3 | 14 | 14
    }

    def "should be able to mock a concrete class"(){
        given:
        Renderer renderer = Mock()
        def polygon = new Polygon(4, renderer)

        when:
        polygon.draw()

        then:
        4 * renderer.getComponent()
    }

}
